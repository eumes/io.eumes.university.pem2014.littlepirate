package io.eumes.university.pem2014.littlepirate.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqLiteHelper extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Translations.sqlite";

    public static final String TABLE_NAME = "translations";
    public static final String COLUMN_NAME_ORIGINAL = "original";
    public static final String COLUMN_NAME_TRANSLATION = "translation";
    public static final String COLUMN_NAME_TIME = "time";

    private static final int MAX_HISTORY_ITEMS = 20;

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + SqLiteHelper.TABLE_NAME + " (" +
                    "_id INTEGER PRIMARY KEY," +
                    SqLiteHelper.COLUMN_NAME_TIME + " LONG," +
                    SqLiteHelper.COLUMN_NAME_ORIGINAL + " TEXT," +
                    SqLiteHelper.COLUMN_NAME_TRANSLATION + " TEXT" +
            ")";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + SqLiteHelper.TABLE_NAME;


    public static final String SQL_DELETE_UNNEEDED_ENTRIES =
            "DELETE FROM " + SqLiteHelper.TABLE_NAME + " " +
            "WHERE _id NOT IN (" +
                "SELECT _id " +
                "FROM " + SqLiteHelper.TABLE_NAME + " " +
                "ORDER BY " + SqLiteHelper.COLUMN_NAME_TIME + " DESC " +
                "LIMIT " + MAX_HISTORY_ITEMS +
                ")";

    public SqLiteHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }
}

