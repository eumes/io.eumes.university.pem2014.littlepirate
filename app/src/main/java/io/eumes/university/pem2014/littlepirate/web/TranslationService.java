package io.eumes.university.pem2014.littlepirate.web;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLDecoder;
import java.net.URLEncoder;

import io.eumes.university.pem2014.littlepirate.model.Translation;
import io.eumes.university.pem2014.littlepirate.web.response.PirateTranslationApiResponse;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class TranslationService {

    private static TranslationService sharedService;
    public static TranslationService getInstance(){
        if (sharedService == null){
            sharedService = new TranslationService();
        }

        return sharedService;
    }


    public interface CompletionCallback {

        public void success(Translation translation);
        public void failure();

    }

    private WeakReference<CompletionCallback> completionCallback;
    public void setCompletionCallback(CompletionCallback completionCallback){
        if (completionCallback == null){
            this.completionCallback = null;
            return;
        }

        this.completionCallback = new WeakReference<>(completionCallback);
    }


    PirateTranslationApi translationApi;
    public TranslationService(){

        //build the rest adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://isithackday.com")
                .build();

        translationApi = restAdapter.create(PirateTranslationApi.class);
    }


    public void getTranslation(String originalText){

        translationApi.getTranslation(originalText, "json", new Callback<PirateTranslationApiResponse>() {
            @Override
            public void success(PirateTranslationApiResponse translationResponse, Response response) {
                if (completionCallback == null || completionCallback.get() == null)
                    return;

                String pirate = translationResponse.translation.pirate;
                String english = translationResponse.translation.english;

                Translation translation = new Translation(english, pirate);
                completionCallback.get().success(translation);
            }

            @Override
            public void failure(RetrofitError error) {
                if (completionCallback == null || completionCallback.get() == null)
                    return;

                completionCallback.get().failure();
            }
        });

    }

}
