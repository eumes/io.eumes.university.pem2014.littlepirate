package io.eumes.university.pem2014.littlepirate.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.eumes.university.pem2014.littlepirate.R;
import io.eumes.university.pem2014.littlepirate.model.Translation;
import io.eumes.university.pem2014.littlepirate.persistence.TranslationRepository;

public class HistoryActivity extends Activity {


    private TranslationArrayAdapter adapter;
    private ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        getActionBar().setTitle("History");

        mapUiElements();
        setupList();

    }

    private void mapUiElements(){
        list = (ListView)findViewById(R.id.history_list);
    }

    private void setupList(){
        adapter = new TranslationArrayAdapter(getApplicationContext());
        list.setAdapter(adapter);
    }


    private class TranslationArrayAdapter extends ArrayAdapter<Translation>{

        private ArrayList<Translation> translations;
        private LayoutInflater inflater;

        public TranslationArrayAdapter(Context context) {
            super(context, 0);
            translations = TranslationRepository.getInstance(context).getTranslations();
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            //check if we can reuse the view
            if(null == convertView) {
                convertView = inflater.inflate(R.layout.list_history_item, null);
            }

            //lookup the text views
            TextView originalText = (TextView)convertView.findViewById(R.id.history_list_item_original);
            TextView translationText = (TextView)convertView.findViewById(R.id.history_list_item_translation);

            //get the translation for this row
            Translation translation = translations.get(position);

            //set the respective texts
            originalText.setText(translation.getOriginal());
            translationText.setText(translation.getTranslation());

            return convertView;
        }

        @Override
        public int getCount() {
            return translations.size();
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }
    }

}
