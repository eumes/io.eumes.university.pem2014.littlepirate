package io.eumes.university.pem2014.littlepirate.model;

public class Translation {

    private long timestamp;
    private String original;
    private String translation;

    public Translation(String originalText, String translationText){
        this(originalText, translationText, System.currentTimeMillis());
    }

    public Translation(String originalText, String translationText, long time){
        original = originalText;
        translation = translationText;
        timestamp = time;
    }

    public String getOriginal() {
        return original;
    }

    public String getTranslation() {
        return translation;
    }

    public long getTimestamp(){
        return timestamp;
    }
}
