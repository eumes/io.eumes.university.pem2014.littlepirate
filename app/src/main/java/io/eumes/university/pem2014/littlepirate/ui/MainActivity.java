package io.eumes.university.pem2014.littlepirate.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import io.eumes.university.pem2014.littlepirate.R;
import io.eumes.university.pem2014.littlepirate.model.Translation;
import io.eumes.university.pem2014.littlepirate.persistence.TranslationRepository;
import io.eumes.university.pem2014.littlepirate.web.TranslationService;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int DRAWER_TRANSLATE_POSITION = 0;
    private static final int DRAWER_HISTORY_POSITION = 1;

    private Button translate;
    private Button clear;

    private EditText original;
    private TextView translation;

    private TranslationRepository repository;
    private TranslationService service;

    private DrawerLayout drawerLayout;
    private ListView drawerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translation);

        getActionBar().setTitle("Translation");

        bindToStatics();
        mapUiElements();
        attachListener();
        setupDrawer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindFromStatics();
    }

    private void bindToStatics(){
        repository = TranslationRepository.getInstance(getApplicationContext());
        service = TranslationService.getInstance();
        service.setCompletionCallback(translationCallback);
    }

    private void unbindFromStatics(){
        service.setCompletionCallback(null);
    }

    private void mapUiElements(){
        translate = (Button)findViewById(R.id.translate);
        clear = (Button)findViewById(R.id.clear);

        original = (EditText)findViewById(R.id.original);
        translation = (TextView)findViewById(R.id.translation);
    }

    private void attachListener(){

        translate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                service.getTranslation(original.getText().toString());

            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                original.setText(null);
                translation.setText(null);

            }
        });

    }

    private void setupDrawer(){

        //load the menu items
        Resources res = getResources();
        String[] drawerItems = res.getStringArray(R.array.drawer);
        ArrayAdapter<String> drawerAdapter = new ArrayAdapter<>(this, R.layout.drawer_list_item, drawerItems);

        //get the drawer
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        //build the list and attach listener
        drawerList = (ListView)findViewById(R.id.left_drawer);
        drawerList.setAdapter(drawerAdapter);
        drawerList.setOnItemClickListener(drawerClickListener);

    }

    private TranslationService.CompletionCallback translationCallback = new TranslationService.CompletionCallback() {
        @Override
        public void success(Translation newTranslation) {
            //update ui
            translation.setText(newTranslation.getTranslation());
            //persist translation
            repository.addTranslation(newTranslation);
        }

        @Override
        public void failure() {
            translation.setText("<failed>");
        }
    };


    private AdapterView.OnItemClickListener drawerClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

            Log.i(TAG, "drawer item " + position + " selected");

            switch (position){
                case DRAWER_TRANSLATE_POSITION:
                    //we currently do not do anything here except closing the drawer
                    drawerLayout.closeDrawers();
                    break;

                case DRAWER_HISTORY_POSITION:
                    //we start the history activity and close the drawer
                    Intent intent = new Intent( MainActivity.this, HistoryActivity.class);
                    MainActivity.this.startActivity(intent);
                    drawerLayout.closeDrawers();
                    break;

            }
        }
    };

}
