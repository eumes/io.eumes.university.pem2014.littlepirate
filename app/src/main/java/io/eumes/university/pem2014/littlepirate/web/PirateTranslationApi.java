package io.eumes.university.pem2014.littlepirate.web;

import io.eumes.university.pem2014.littlepirate.web.response.PirateTranslationApiResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface PirateTranslationApi {

    @GET("/arrpi.php")
    public void getTranslation(@Query("text") String text, @Query("format") String format, Callback<PirateTranslationApiResponse> callback);

}
