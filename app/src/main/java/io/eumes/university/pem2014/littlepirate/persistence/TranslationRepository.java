package io.eumes.university.pem2014.littlepirate.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import io.eumes.university.pem2014.littlepirate.model.Translation;

public class TranslationRepository {

    private static TranslationRepository sharedRepository;
    public static TranslationRepository getInstance(Context context){
        if (sharedRepository == null){
            sharedRepository = new TranslationRepository(context);
        }

        return sharedRepository;
    }

    private Object lock;
    private SqLiteHelper helper;
    private SQLiteDatabase database;
    public TranslationRepository(Context context){
        helper = new SqLiteHelper(context);
        database = helper.getWritableDatabase();
        lock = new Object();
    }

    public void addTranslation(Translation translation){

        //create value
        ContentValues values = new ContentValues();
        values.put(SqLiteHelper.COLUMN_NAME_TIME, translation.getTimestamp());
        values.put(SqLiteHelper.COLUMN_NAME_ORIGINAL, translation.getOriginal());
        values.put(SqLiteHelper.COLUMN_NAME_TRANSLATION, translation.getTranslation());

        //insert them
        synchronized (lock){
            database.insert(SqLiteHelper.TABLE_NAME, null, values);
            deleteOldEntries();
        }

    }

    public ArrayList<Translation> getTranslations(){

        ArrayList<Translation> translations = new ArrayList<>();

        //create query
        String[] columns = {
            SqLiteHelper.COLUMN_NAME_TIME,
            SqLiteHelper.COLUMN_NAME_ORIGINAL,
            SqLiteHelper.COLUMN_NAME_TRANSLATION
        };
        String order = SqLiteHelper.COLUMN_NAME_TIME + " DESC";

        //query translations
        synchronized (lock) {
            Cursor cursor = database.query(SqLiteHelper.TABLE_NAME, columns, null, null, null, null, order);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Translation translation = translationFromCursor(cursor);
                translations.add(translation);
                cursor.moveToNext();
            }
            cursor.close();
        }

        return translations;
    }

    private Translation translationFromCursor(Cursor cursor){

        //get the three items and convert them to a translation
        long timestamp = cursor.getLong(0);
        String originalText = cursor.getString(1);
        String translationText = cursor.getString(2) ;

        Translation translation = new Translation(originalText, translationText, timestamp);
        return translation;
    }

    private void deleteOldEntries(){
        //delete unneeded entries
        database.execSQL(SqLiteHelper.SQL_DELETE_UNNEEDED_ENTRIES);
    }

}
